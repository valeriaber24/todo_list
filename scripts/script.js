function createListItem(value) {
    const listItem = document.createElement("div");
    listItem.classList.add("list__flex-between");

    const itemValue = document.createElement("textarea");
    itemValue.classList.add("list__item", "list__item-input");
    itemValue.value = value;
    listItem.appendChild(itemValue);

    const doneButton = document.createElement("button");
    doneButton.classList.add("list__button__done");
    listItem.appendChild(doneButton);

    const deleteButton = document.createElement("button");
    deleteButton.classList.add("list__button__delete");
    listItem.appendChild(deleteButton);

    return listItem;
}

document.addEventListener("input", (event) => {
    if (event.target.classList.contains("list__item-input")) {
        const input = event.target;
        const inputWidth = input.offsetWidth;
        const fontSize = window.getComputedStyle(input).fontSize;
        const canvas = document.createElement("canvas");
        const context = canvas.getContext("2d");
        context.font = fontSize;
        const textWidth = context.measureText(input.value).width;

        if (textWidth > inputWidth) {
            input.value += "\n";
        }
    }
});

function addListItem() {
    const inputValue = document.querySelector(".list__input").value.trim();
    if (inputValue !== "") {
        const listItem = createListItem(inputValue);
        document.querySelector(".list_container").appendChild(listItem);

        const expirationDate = new Date();
        expirationDate.setDate(expirationDate.getDate() + 1);
        document.cookie = `listItem=${encodeURIComponent(inputValue)}; expires=${expirationDate.toUTCString()}`;
    }
}

document.querySelector(".list__button__edit").addEventListener("click", () => {
    const inputValue = document.querySelector(".list__input").value.trim();
    if (inputValue !== "") {
        addListItem();
        document.querySelector(".list__input").value = "";
    }
});

document.querySelector(".list__input").addEventListener("keydown", (event) => {
    if (event.key === "Enter") {
        addListItem();
        document.querySelector(".list__input").value = "";
    }
});

function deleteListItem(listItem) {
    listItem.remove();

    document.cookie = `listItem=; expires=${new Date(0).toUTCString()}`;
}

document.addEventListener("click", (event) => {
    if (event.target.classList.contains("list__button__delete")) {
        const listItem = event.target.closest(".list__flex-between");
        deleteListItem(listItem);
    }
});

function markAsDone(listItem) {
    listItem.classList.add("list__item--done");
}

document.addEventListener("click", (event) => {
    if (event.target.classList.contains("list__button__done")) {
        const listItem = event.target.closest(".list__flex-between");
        markAsDone(listItem);
        listItem.remove();
    }
});

function clearList() {
    const listContainer = document.querySelector(".list_container");
    const listItems = listContainer.querySelectorAll(".list__item, .list__button__done, .list__button__delete");
    listItems.forEach(item => item.remove());

    document.cookie = `listItem=; expires=${new Date(0).toUTCString()}`;
}

document.querySelector(".list__button__clear").addEventListener("click", clearList);

const cookies = document.cookie.split("; ");
const listItemCookie = cookies.find((cookie) => cookie.startsWith("listItem="));

if (listItemCookie) {
    const listItemValue = decodeURIComponent(listItemCookie.split("=")[1]);
    const listItem = createListItem(listItemValue);
    document.querySelector(".list_container").appendChild(listItem);
}

